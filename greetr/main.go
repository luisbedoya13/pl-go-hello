package main

import (
	"fmt"

	"hello_lib"
)

func main() {
	greetings := hello_lib.Hello("English", "Spanish")
	fmt.Printf("In English is %s, en Español es %s\n", greetings[0], greetings[1])
}
