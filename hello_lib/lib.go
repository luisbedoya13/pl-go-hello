package hello_lib

var greetings = map[string]string{
	"English": "Hello world",
	"French":  "Salut monde",
	"Spanish": "Hola mundo",
}

func Hello(langs ...string) []string {
	if len(langs) == 0 {
		return []string{greetings["English"]}
	}
	var results []string
	for _, lang := range langs {
		if greeting, ok := greetings[lang]; ok {
			results = append(results, greeting)
		}
	}
	return results
}
