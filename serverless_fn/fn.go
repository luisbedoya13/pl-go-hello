package serverless_fn

import (
	"encoding/json"
	"fmt"
	"hello_lib"
	"html"
	"net/http"
)

func HelloHTTP(rw http.ResponseWriter, r *http.Request) {
	var d struct {
		Language string `json:"lang"`
	}
	if err := json.NewDecoder(r.Body).Decode(&d); err != nil {
		fmt.Fprint(rw, "I can't understand you")
		return
	}
	lang := html.EscapeString(d.Language)
	fmt.Fprintf(rw, "Your greeting is: %s", hello_lib.Hello(lang))
}
